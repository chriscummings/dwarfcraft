var engine;

$(document).ready(function() {

	engine = new Engine();

	// Spacebar to start/stop game
	var gamePaused = true;
	$(document).keyup(function(evt) {
		if (evt.keyCode == 32) {
			if(gamePaused === true){
				gamePaused = false;
				engine.start();
			}else{
				gamePaused = true;
				engine.stop();
			}
		}
	});


});


