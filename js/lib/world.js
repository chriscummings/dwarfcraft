/**/
function World(){
	this.entities = [];
	
	for(var i=0; i<600; i++){
		this.entities.push(new AmericanDingo(450, 450));
	}
	
	return this;
}

World.prototype = {
	tick: function(){
		
		var entitiesLength = this.entities.length;
		for(var i=0; i<entitiesLength; i++){
			var entity = this.entities[i];
			
			entity.tick();
		}
		
		this.draw();
	},
	
	draw: function(){
		/*!
		 * Run benchmarks between using fragments or just string concat to  
		 * see which is faster for appending all elements at once.
		 * http://www.bennadel.com/blog/2281-jQuery-Appends-Multiple-Elements-Using-Efficient-Document-Fragments.htm
		 */
		
		$('body').empty();
		

		
		var entitiesLength = this.entities.length;
		for(var i=0; i<entitiesLength; i++){
			var entity = this.entities[i];
			
			var element = $('<div>')
				.css({
					'position':'absolute',
					'left': entity.x,
					'top': entity.y,
					'height':'5px',
					'width':'5px',
					'background':'#000',
					'display':'block'
				});
			
			$('body').append(element);
		}
	}
};