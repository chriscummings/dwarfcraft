/*!
 * Entity class relies on the Klass library (https://github.com/ded/klass) to 
 * make life a little easier given the need for inheritance of mob classes. 
 * In the future I may implement honest prototypal inheritance. 
 */

var Entity = klass(function(x, y){
	this.x = x;
	this.y = y;
	// This doesn't work as a way for subclasses to talk amongst themselves.
	//this.registerSibling();
})
.statics({
	// This doesn't work as a way for subclasses to talk amongst themselves.
	//siblings: []
})
.methods({
	tick: function(){}
	
	// This doesn't work as a way for subclasses to talk amongst themselves.
	// registerSibling: function(){
	// 	var self = this;
	// 	Entity.siblings.push(self);
	// },
	// This doesn't work as a way for subclasses to talk amongst themselves.
	
	// getSiblingCount: function(){
	// 	return Entity.siblings.length;
	// }
});

var AmericanDingo = Entity.extend(function(x, y){
	
})
.methods({
	
	wander: function(){
		// Random -10 to +10
		var randomNumber1 = Math.floor(Math.random() * 21) - 10;
		var randomNumber2 = Math.floor(Math.random() * 21) - 10;
		
		this.x += randomNumber1;
		this.y += randomNumber2;
		
	},
	
	tick: function(){
		this.wander();
	}
});