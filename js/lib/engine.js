/**/
function Engine(world, timer){
	
	this.world = new World();
	this.tickCount = 0;
	
	// Preserve context
	var self = this;	
	this.timer = new Timer({
	    fps: 30,
	    run: function(){
	        self.tick();
	    }
	});
	
	return this;
}

Engine.prototype = {
	start: function(){
		this.timer.start();
	},
	
	stop: function(){
		this.timer.stop();
	},
	
	tick: function(){
		this.tickCount++;
		//console.log(this.tickCount + ' tick');
		this.world.tick();
		
	}
	
	
};