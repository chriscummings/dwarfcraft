function Map(world){
	this.world = world;
	this.tiles = [];
	
	return this;
}

Map.prototype = {
	
	render: function(){
		
		var html = '';
		
		$('body').empty();
		
		var tilesWidthLength = this.tiles.length;
		for(var x=0; x< tilesWidthLength; x++){
			var row = this.tiles[x];
			
			var tilesHeightLength = row.length;
			for(var y=0; y<tilesHeightLength; y++){
				var tile = row[y];
				tile.render();
				
				
			}
		}
		
		
		
		
	},
	
	generate: function(){
		var tiles = [];
		
		var xWide = 30;
		var yWide = 30;
		
		// Set up x columns
		for(var x=0; x<xWide; x++){
			
			var row = [];
			for(var y=0; y<yWide; y++){
				row.push(new Tile(x, y));
			}
			tiles.push(row)
			
		}
		

		
		this.tiles = tiles;
	}

};