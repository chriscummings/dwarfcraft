/**/
function World(){
	this.map = new Map(this);
	this.map.generate();
	
	this.entities = [];
	for(var i=0; i<0; i++){
		this.entities.push(new AmericanDingo(1, 1, null, this));
	}
	
	return this;
}

World.prototype = {
	
	isTileWalkable: function(x, y){
		
		// Check map tile type
		if(this.map.tiles[x][y] !== 0){
			
			// Check if tile is occupied
			var entityLength = this.entities.length;
			for(var i=0; i<entityLength; i++){
				var entity = this.entities[i];
				if(entity.position.y === y && entity.position.x === x){
					console.log('Bump')
					return false;
				}
			}
			
			return true
		}
	},
	
	tick: function(){
		
		// Do all entity actions
		var entitiesLength = this.entities.length;
		for(var i=0; i<entitiesLength; i++){
			var entity = this.entities[i];
			entity.tick();
		}
		
		this.render();
	},
	
	render: function(){
		/*!
		 * Run benchmarks between using fragments or just string concat to  
		 * see which is faster for appending all elements at once.
		 * http://www.bennadel.com/blog/2281-jQuery-Appends-Multiple-Elements-Using-Efficient-Document-Fragments.htm
		 */
		
		$('body').empty();
		this.map.render();
		
		var entitiesLength = this.entities.length;
		for(var i=0; i<entitiesLength; i++){
			var entity = this.entities[i];
			entity.render();
		}

	}
};