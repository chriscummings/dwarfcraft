/*!
 * Entity class relies on the Klass library (https://github.com/ded/klass) to 
 * make life a little easier given the need for inheritance of mob classes. 
 * In the future I may implement honest prototypal inheritance. 
 */

var Entity = klass(function(x, y, z, world){
	this.world = world;
	this.position = new Position(x, y, z)
	this.movement = new Movement(this);
})
.statics({
	//
})
.methods({
	isTileWalkable: function(x, y){
		return this.world.isTileWalkable(x, y);
	},
	
	tick: function(){},
	render: function(){
		
		
		var html = '<div class="tile mob" style="top:'+ (this.position.y*20) +'; left:'+ (this.position.x*20) +';"></div>';
		
		$('body').append(html);
	}
});

var AmericanDingo = Entity.extend(function(x, y, z){
	//
})
.methods({
	tick: function(){
		this.movement.wander();
	}
});