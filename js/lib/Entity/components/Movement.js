function Movement(entity){
	this.entity = entity
	
	return this;
}

Movement.prototype = {
	wander: function(){
		var randomNumber = Math.floor(Math.random() * 4);

		var possibleX = this.entity.position.x;
		var possibleY = this.entity.position.y;
		
		switch(randomNumber){
		case 0:
			possibleX = this.entity.position.x - 1
			break;
		case 1:
			possibleX = this.entity.position.x + 1
			break;
		case 2:
			possibleY = this.entity.position.y - 1
			break;
		case 3:
			possibleY = this.entity.position.y + 1
			break;
		}

		// Check x direction
		if(possibleX > -1 && possibleX > -1){
			if(this.entity.isTileWalkable(possibleX, possibleY) === true){
				this.entity.position.x = possibleX;
				this.entity.position.y = possibleY;
			}			
		}

	}
	
};

